class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|
      t.integer :id
      t.integer :c_id
      t.integer :from_pin
      t.integer :to_pin
      t.decimal :for_250gms
      t.decimal :for_500gms
      t.decimal :for_1kg

      t.timestamps
    end
  end
end
