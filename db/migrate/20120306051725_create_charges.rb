class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.integer :id
      t.integer :courier_id
    
      t.integer :from_pin
      t.integer :to_pin
      t.decimal :for_250gms
      t.decimal :for_500gms
      t.decimal :for_kg
      

      t.timestamps
    end
  end
end
