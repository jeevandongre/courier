class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :c_id
      t.string :c_name
      t.decimal :fuel_surcharge
      t.decimal :tax
      t.decimal :weight
      t.decimal :cod_charge

      t.timestamps
    end
  end
end
