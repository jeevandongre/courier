# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120306052528) do

  create_table "charges", :force => true do |t|
    t.integer  "courier_id"
    t.integer  "from_pin"
    t.integer  "to_pin"
    t.decimal  "for_250gms", :precision => 10, :scale => 0
    t.decimal  "for_500gms", :precision => 10, :scale => 0
    t.decimal  "for_kg",     :precision => 10, :scale => 0
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  create_table "courier_details", :force => true do |t|
    t.string   "courier_name"
    t.decimal  "fuel_surcharge", :precision => 10, :scale => 0
    t.decimal  "weight",         :precision => 10, :scale => 0
    t.decimal  "tax",            :precision => 10, :scale => 0
    t.decimal  "cod_charges",    :precision => 10, :scale => 0
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
  end

  create_table "details", :force => true do |t|
    t.integer  "c_id"
    t.integer  "from_pin"
    t.integer  "to_pin"
    t.decimal  "for_250gms", :precision => 10, :scale => 0
    t.decimal  "for_500gms", :precision => 10, :scale => 0
    t.decimal  "for_1kg",    :precision => 10, :scale => 0
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  create_table "posts", :force => true do |t|
    t.integer  "c_id"
    t.string   "c_name"
    t.decimal  "fuel_surcharge", :precision => 10, :scale => 0
    t.decimal  "tax",            :precision => 10, :scale => 0
    t.decimal  "weight",         :precision => 10, :scale => 0
    t.decimal  "cod_charge",     :precision => 10, :scale => 0
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
  end

  create_table "rate_details", :force => true do |t|
    t.string   "courier_id"
    t.string   "from_pin"
    t.string   "to_pin"
    t.string   "weight_250"
    t.string   "weight_500"
    t.string   "weight_1kg"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
